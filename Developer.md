# Workflow

## Expectations
As a developer at REAK:

* You're always striving to improve yourself.
* You take ownership of the tasks you receive, which means making sure that the task will be completed within the schedule and safely delivered on production, even if requirements get slightly adjusted mid-term.
* You will learn and understand the code completely, to improve it over time (simplified, more performing, easier to maintain and read).
* You will receive user requirements (from the client's end along with notes from senior engineer(s) if needed), your responsibility is to make sure to clearly understand the whole picture, before translating them into technical steps to implement following the current codebase infrastructure.
* You will think rationally and logically, and take initiatives to adjust the requirements whenever it makes sense (with the knowledge that nobody else knows about the system), and accordingly let me know about the changes to confirm with the sales team whether it is acceptable or not.
* You will add unit tests to cover the code that you're modifying, migration files if needed, keep a release log for the system administrator to correctly apply your changes to the production server.
* You will communicate regularly, for senior developer(s) to keep track of your progress (including notifying when you start/stop working).
* You will adhere to our technical workflow conventions (git branching flow, pull request procedure, etc).
* Senior Developer's involvement will be minimal: Help you understand the client's requirements, forward back and forth your questions and their answers, and review your code to insure its quality.

## Flow
General flow:

* Before starting a task, you'll be asked to evaluate how much time is required for you to complete it (including chores such as writing unittests and adjusting your pull requests from code reviews).
* The time estimation and completing within the stipulated time will get you points in the appraisal season as all these units are logged in our performance review system.

Detailed flow:

1. You assign yourself to the task you're starting (if it wasn't already done for you)
2. You add the label WIP to the issue when you start working on it (and remove the label when the task is completed)
3. You write down a comprehensive todo list before getting started with the task, to be methodical (use markdown syntax in gitlab comments)
4. You work on each part in git feature branches, and create pull requests on Gitlab for each feature (small PRs are best, easier to review and faster to merge and deploy to production). Make sure that each PR has a release notes & deployment steps attached to help me correctly apply these changes on production.
5. You assign any of the available Senior Developer(s) to review the PR and wait for approval
6. If Senior Developer(s) asks questions on the PR, please answer them soon (make sure you quickly receive notifications from gitlab). If changes are needed, please fix them (or argument or why we shouldn't fix one)
7. Make sure all the discussions are resolved if they were raised by Senior Developer(s)
8. Senior Developer(s)  approves the PR and you merge it back into master (from gitlab by pressing the "Merge pull request" button)


## Conventions

### Git
* Create Issue on GitLab
* Write Detailed Todo List on Issue (Checklist)
* Click on Create Merge Request button on Gitlab
* This would create branch for the issue as well a PR
* By default all PR are started with WIP prefix
* Push out commits and when done,
* Go into PR (Merge Request) and assign Senior Developer(s) to the PR and remove WIP with Resolve (option is there to mark PR resolve)
* Resolve any discussion / points raised by Senior Developer(s)
* Once Merged, ensure master branch is working fine, if not Please raise the issue with Senior Developer(s) who merged your branch.



### Update local db:

* Make sure all your projects have a SQL folder
* Inside that folder should be two-subfolders, Latest & Legacy
* Latest should contain the entire DB dump
* Legacy should have timestamped DB's for logging purposes
* Everytime a new Latest DB is placed, the old copy should be moved to Legacy with Timestamp.
