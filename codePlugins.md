## Install VSCode Plugins

Copy and paste the entire commands inside terminal.

```
code --install-extension PeterJausovec.vscode-docker
code --install-extension Zignd.html-css-class-completion
code --install-extension alefragnani.project-manager
code --install-extension dbaeumer.vscode-eslint
code --install-extension eg2.tslint
code --install-extension esbenp.prettier-vscode
code --install-extension fabiospampinato.vscode-todo-plus
code --install-extension felixfbecker.php-intellisense
code --install-extension formulahendry.auto-close-tag
code --install-extension formulahendry.code-runner
code --install-extension kokororin.vscode-phpfmt
code --install-extension ms-python.python
code --install-extension ms-vscode.cpptools
code --install-extension robertohuertasm.vscode-icons
code --install-extension tomoki1207.vscode-input-sequence
```