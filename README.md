![REAK Logo](logo/reak_logo.png)

Welcome
=======

Here at our organization things are done a bit differently, and might be a different ball game altogether from your previous employment.

To start with we don’t really have any working hours, All we expect from you is a solid 8-hour working schedule, deliver quality work in that time. Be it 10 AM to 6 PM or be it 12PM to 8 PM.

#### Learning Curve

You will face a very strong learning curve here even with the language you’re proficient with, All we ask from you is to be patient and hungry to learn for the initial months to grasp on how we work and how we expect you to work.

We don’t really have different teams for different tasks, Frontend, Backend, Devops, Testing etc. We want all of the employees here at REAK to crossover and have basic knowledge in all the fields and do what interests you but if the time comes for all hands on deck, we want each and everyone to be a part of it.

#### Basic Requirements

Here are the basic requirements we expect from anyone starting work here, If you are not familiar with anything written below please let us know and we can guide you in the right direction.

*   Basic understanding of how Internet works, DNS, WebServers, FTP Servers, SSH and other internet / web administrator related concepts.
*   Linux, What are distributions, What is Linux. Terminal and Bash concepts, Regular Copy, Move, Create, Traverse commands.
*   Basic Understanding of how security works for anything web related, What are exploits, What are the different ways to gain access to a system.
*   Basic Understanding of Data Structure, Classes, Objects
*   Understanding of calculating complexity of code and how code can be optimized further
*   Understanding on when and how to deal with Exceptions
*   Basic Understanding of GIT, OOPS methodology, MVC Architecture, MySQL

#### Important Links

We assume you have read all of the above instructions and are ready to start your career here at REAK, But before you start please go through the following :

*   Please fill out the form provided below, This provides us with your basic medical information for First Aid purposes and to take note of your preferences during events. 
    [Employee Information Form](https://docs.google.com/forms/d/e/1FAIpQLSf71_sPXgjEFTMx-lzwp1_K-UpeZKnZ3xCov1f4ZxHHUXYyMg/viewform)
    
*   Anonymous Feedback Form, Please use this to place any request or if you want to report any workplace instance. You can leave your details for us to further followup on the complaint, but it’s not required. 
    [Leave Feedback](https://docs.google.com/forms/d/e/1FAIpQLSdyCqgU3HaxUaeAtIAOQiGmpk5DnD72sFUC9R1c8FlQKJBX3Q/viewform?usp=sf_link)
    
*   Code of Conduct, Employee Guidelines and Privacy at REAK can be accessed from below
    [Code of Conduct, Guidelines and Privacy](Guidelines.md)

*   Your personal devices can be registered on Office's Wifi, For this you will have to fill in the following form
    [Register Personal Device](https://forms.zohopublic.com/virtualoffice12009/form/DeviceNetworkRegistration/formperma/C-Y8HjzWLlQqYsZvA37H6QTljOtT1lUP-ZtrBhiTV58)
    

#### Recommended Tools

*   Editor/IDE - Visual Studio Code
    [Recommended Plugins for Code](codePlugins.md)
*   GitKraken - GUI Git Client
*   Slack - Inter Team chat
*   Asana - Task Scheduling & Project Management
*   Zoho - Organization Email
*   Google Authenticator - 2Factor Auth

#### Resources

*   [Developer Doc](Developer.md)
*   [System Setup](SystemSetup.md)

#### Learning Resources

*   [Basic Git Explaination](https://www.youtube.com/watch?v=8KCQe9Pm1kg)
*   [Concepts of Business Communication](https://www.youtube.com/watch?v=sFp5LPJ69EI&list=PLxSz4mPLHWDb5ilkSD089gY2UwKiP72S7)
*   [Git Cheat Sheet](http://ocw.udl.cat/enginyeria-i-arquitectura/software-quality/git-cheat-sheet-large)
*   [Git Command Sheet](https://www.git-tower.com/blog/content/posts/54-git-cheat-sheet/git-cheat-sheet-large01.png)