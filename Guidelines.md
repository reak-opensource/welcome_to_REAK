![REAK Logo](logo/reak_logo_small.png  "REAK Logo")

# Code of Conduct, Employee Guidelines & Privacy

## Encouraged Behaviour

* Be Welcoming
* Be Kind
* Look out for each other


## Unacceptable Behaviour

* Conduct or speech which might be considered sexist, racist, homophobic, transphobic, ableist or otherwise discriminatory or offensive in nature.
* Do not use unwelcome, suggestive, derogatory or inappropriate nicknames or terms.
* Do not show disrespect towards others. (Jokes, innuendo, dismissive attitudes.)
* Intimidation or harassment (online or in-person).
* Disrespect towards differences of opinion.
* Inappropriate attention or contact. Be aware of how your actions affect others. If it makes someone uncomfortable, stop.
* Not understanding the differences between constructive criticism and disparagement
* Sustained disruptions
* Violence, threats of violence or violent language.

## Reporting Violation of Code of Conduct

Any violations of the above can be reported anonymously to
[Report Anonymously](https://docs.google.com/forms/d/e/1FAIpQLSdyCqgU3HaxUaeAtIAOQiGmpk5DnD72sFUC9R1c8FlQKJBX3Q/viewform?usp=sf_link)


## Dress, Time & General Policy
* We do not have a dress code, so formals / casuals should be fine as long as they are decent.
* For the sake of it, we have a routine of 10 AM to 6PM. It would benefit if you can join in the same time window but if not, all we care about is quality work and you putting in 8 hours of grind be it for any time.
* Leaves, This is something we believe shouldn't require anybody's permission. If you have to go there's no reason of holding you back. We just ask from you to resolve any deadlines before hand which you have for that day. Also if you are taking a leave to watch a match or the major is on, Feel free to start streaming right here in office.
* Follow general etiquettes, Keep your phone on silent, Please move out of the working floor if you want to have a phone call, Use headphones while listening to music.
* We do not appreciate taking personal work calls during office hours.
* All employees can avail cab / drop facilities to their home in case they are working late (post 8:30 PM)
* Please keep the bathroom clean and maintain general and personal hygiene.


## Privacy Policy & Declarations
* Office is under CCTV survillience and all office activity gets recorded and is kept for a period of minimum 6 months.
* Your office computers are synced with Master, to log in your work activity, programs you have and your work history.
* Your work emails are private and will remain the same unless we have a complaint or strong suspicion of foul play at which time we will access your account for further information.
* Some programs have tracers or beacon code and if syncing to your personal device, we might be able to see some personal information.
* Transmitting REAK's IP to any 3rd party non-employee will be considered a crime with zero tolerance, Instant FIR will be filed against you along with immediate termination and access revokation.
* Office network is monitored for malicious traffic, any personal information captured because of this either from office systems or personal devices is logged.
* In case of strong suspicion of fraud, The company (REAK INFOTECH LLP) reserves the right to use any of the infrastructure available including personal documents submitted to investigate. This includes but is not limited to CCTV Footage and Audio within and outside premise, Documents submitted like Bank Details, Driving License and your system or devices which are within premises or are running REAK's tech stack.