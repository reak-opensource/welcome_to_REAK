## Setup System

* Update & Upgrade Packages
```sudo apt-get update && sudo apt-get upgrade```

* Install Apache, PHP & MySQL
```sudo apt-get install lamp-server^```

* Install PHPMyAdmin
```sudo apt-get install phpmyadmin```

* Install Phalcon
```
curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | sudo bash
sudo apt-get install php7.0-phalcon
```

* Setup Apache Rewrite
```sudo a2enmod rewrite```

* Restart Apache
```sudo service apache2 restart```

* Set Apache & MySQL to autostart
```
update-rc.d apache2 defaults
update-rc.d mysql defaults
```

* Install pip and virtualenv for Python
```sudo apt-get install python-setuptools python-dev build-essential virtualenv```

* Install REAK devtool
```
wget https://gitlab.com/reak-opensource/reak-devtools/raw/master/install.sh
chmod +x install.sh
./install.sh
```

* Install gDebi to easily install .deb packages
```sudo apt-get install gdebi```


* Install gitkraken
Download here : https://www.gitkraken.com/download/linux-deb
Install with gDebi (Right click in FIle manager, Open With gdebi)

* Install Visual Studio Code
```
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get update
sudo apt-get install code
```

* Install Slack Desktop Client
```sudo snap install slack ```


## Notes

* For Linux system, You are using Xubuntu (Ubuntu with XFCE Interface)
* While downloading applications, look for 64-bit .deb files
* Download those files and open them up with gdebi installer 